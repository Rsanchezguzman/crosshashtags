import numpy as np
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neighbors import NearestNeighbors
from scipy.spatial import distance
from os import listdir
from utils.evaluator import Evaluator

import re
import json
from tqdm import tqdm

# For tokenization
try:
  # UCS-4
  EMOTICON = re.compile(u'(([\U00002600-\U000027BF])|([\U0001f300-\U0001f64F])|([\U0001f680-\U0001f6FF]))')
except Exception, e:
  # UCS-2
  EMOTICON = re.compile(u'(([\u2600-\u27BF])|([\uD83C][\uDF00-\uDFFF])|([\uD83D][\uDC00-\uDE4F])|([\uD83D][\uDE80-\uDEFF]))')
NOT_EMOTICON = re.compile(r'(\\U([0-9A-Fa-f]){8})|(\\u([0-9A-Fa-f]){4})')

def main():
  
  ## KNN_IMG_1
  train_tokens = {}
  test1_tokens = {}

  # Cargamos el archivo de train de Instagram
  with open("test-captions/original/insta-caption-train.json", 'r') as f:
    train_json = json.load(f)
  # Cargamos el archivo de test de Instagram
  with open("test-captions/original/insta-caption-test1.json", 'r') as f: 
    test1_json = json.load(f)

  # Train data
  for user_id, posts in tqdm(train_json.items(), ncols=70, desc="train data"):
    train_tokens[user_id] = {}
    for post_id, post in posts.items():
      post_tokens = post['caption']
      train_tokens[user_id][post_id] = tokenize(post_tokens)

  # Test1 data
  for user_id, posts in tqdm(test1_json.items(), ncols=70, desc="test1 data"):
    test1_tokens[user_id] = {}
    for post_id, post in posts.items():
      post_tokens =  post['caption']
      test1_tokens[user_id][post_id] = tokenize(post_tokens)

  with open("test-captions/original/train.txt", "r") as archivo_palabras:
     contenido = archivo_palabras.read()

  #longitud_train = len(contenido.splitlines())


  n_train = 648760 # Numero de posts de entrenamiento
  matriz_train = range(n_train)
  clase_train = range(n_train)

  name_files_train = contenido.splitlines()[:n_train]

  # Cargamos las matrices de train generadas por la red resnet
  for i,linea in enumerate(name_files_train):
    name = linea.split(",")[0]
    matriz_train[i] = np.load('../attend2u/data/resnet_pool5_features/' + name)
    clase_train[i] = name
    print("Train ", i)


  with open("test-captions/original/test1.txt", "r") as archivo_palabras:
     contenido = archivo_palabras.read()

     
  longitud_test = len(contenido.splitlines())
  matriz_test= range(longitud_test)
  clase_test = range(longitud_test)

  name_file_test = contenido.splitlines()

  # Cargamos las matrices de test generadas por la red resnet
  for i,linea in enumerate(name_file_test):
    name = linea.split(",")[0]
    matriz_test[i] = np.load('../attend2u/data/resnet_pool5_features/' + name)
    clase_test[i] = name

  train_d = np.array(matriz_train)
  test_d = np.array(matriz_test) 
  
  nsamples, nx, ny, nz = train_d.shape
  d2_train_dataset = train_d.reshape((nsamples,nx*ny*nz))  
 
  # Aprendizaje supervisado 1 vecino
  knn = KNeighborsClassifier(n_neighbors = 1)
  knn.fit(d2_train_dataset, clase_train)

  nsamples, nx, ny, nz = test_d.shape
  d2_test_dataset = test_d.reshape((nsamples,nx*ny*nz))  
 
  desc_token_list = []
  answer_token_list = []

  # Realizamos la prediccion
  for i, d2_test in enumerate(d2_test_dataset):
    print("test ", i) 

    predi = knn.predict(d2_test)
    name_prediccion = predi[0]
    user_id_train = name_prediccion.split("_")[0]
    post_id_train = "_".join(name_prediccion.split("_")[2:5]).replace(".npy","")

    name_target = clase_test[i]
    user_id_test = name_target.split("_")[0]
    post_id_test  = "_".join(name_target.split("_")[2:5]).replace(".npy","")

    desc_token_list.append(train_tokens[user_id_train][post_id_train])
    answer_token_list.append(test1_tokens[user_id_test][post_id_test])  
    
  # Evaluamos los subtitulos  
  eval_caption(desc_token_list, answer_token_list)  
 

  # APrendizaje no supervisado
  # https://scikit-learn.org/stable/modules/neighbors.html
  #nbrs = NearestNeighbors(n_neighbors=2, algorithm='ball_tree').fit(d2_train_dataset)
  #distances, indices = nbrs.kneighbors(d2_train_dataset)


def eval_caption(descripciones_finales, descripciones_objetivo):
  # Para que funcione la metrica METEOR el idioma debe estar LANG=en_US.UTF-8
  # En caso de ver este error ValueError: could not convert string to float: 
  evaluator = Evaluator()
  result = evaluator.evaluation(descripciones_finales, descripciones_objetivo, "coco")

  
def tokenize(sentence):
  """Tokenize a sentence"""
  if isinstance(sentence, list):
    sentence = ' '.join(sentence)

  sentence = sentence.replace('#', ' #')
  sentence = sentence.replace('@', ' @')
  sentence = sentence.replace('\n', ' ')
  sentence = sentence.lower()
  sentence = re.sub(r'@[a-zA-Z0-9._]+', '@username', sentence)  # change username
  sentence = EMOTICON.sub(r'@@byeongchang\1 ', sentence)
  sentence = sentence.encode('unicode-escape')  # for emoticons
  sentence = re.sub(r'@@byeongchang\\', '@@byeongchang', sentence)
  sentence = NOT_EMOTICON.sub(r' ', sentence)
  sentence = re.sub(r"[\-_]", r"-", sentence)  # incoporate - and _
  sentence = re.sub(r"([!?,\.\"])", r" ", sentence)  # remove duplicates on . , ! ?
  sentence = re.sub(r"(?<![a-zA-Z0-9])\-(?![a-zA-Z0-9])", r"", sentence)  # remove - if there is no preceed or following
  sentence = ' '.join(re.split(r'[^a-zA-Z0-9#@\'\-]+', sentence))
  sentence = re.sub(r'@@byeongchang', r' \\', sentence)
  return sentence.split()

  
if __name__== "__main__":
  main()


