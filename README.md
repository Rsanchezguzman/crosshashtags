
# crossHashtags #

## Descripción

La librería crossHashtags tiene como función principal la predicción hashtags en dominios cruzados como Twitter e Instagram mediante el uso de redes neuronales (attend2u) y algoritmos KNN para su posterior evaluación.

## Referencia

```
@thesis{crossHashtags:2019,
    author    = {Ricardo Sánchez-Guzmán},
    supervisor= {Alejandro Bellogín},
    title     = {Estudio y aplicación de redes neuronales a predicción de hashtags en dominios cruzados},
    school    = {Escuela Politecnica Superior, Universidad Autonoma de Madrid},
    type      = {Bachelor's Thesis},
    year      = 2019
}
```

## Trabajado Fin De Grado
 

[Estudio y aplicación de redes neuronales a predicción de hashtags en dominios cruzados](https://bitbucket.org/Rsanchezguzman/crosshashtags/downloads/TFG_Ricardo.pdf).


## Descargar repositorio

`git clone git@bitbucket.org:Rsanchezguzman/crosshashtags.git`

## Prerrequisitos

1.Instalar los paquetes necesarios para la ejecución de la librería attend2u.

`python -r attend2u/requirements.txt`

2.Descargar la red pre-entreneada resnet101 y descomprimir.

`wget http://download.tensorflow.org/models/resnet_v1_101_2016_08_28.tar.gz`

 Descomprimir red pre-entrenada en __attend2u/scripts/__.

`tar -xvf resnet_v1_101_2016_08_28.tar.gz`

3.Descargar imagenes-Twitter y descomprimir.

`https://mega.nz/#!I51UECiC!fPSFDzF2AMZHatwNOLAwrDIwQohG_rsebHpW3K16y0g`

Descomprimir imágenes de twitter en __api_twitter/__.

4.Descargar archivos json de los usuario de Twitter y descomprimir.

`https://mega.nz/#!B5U3jKLa!TCydWTK9KtPcwPpBdfKnSTTvAoxhFCYuvjtInSSP1N8`

Descomprimir en en __api_twitter/__.

5.Descargar la carpeta data con todos los archivos de test y descomprimir.

`https://mega.nz/#!0hFDHKLJ!E_09QKJYwdkQ1M4z2xdyZBCoZnM1eAmgjEk-xW4_HUY`

Descomprimir en __attend2u/__.

6.Descargar imágenes de Twitter e Instagram.

`https://mega.nz/#!50ty1agb!xsUcJtuVgBSjUJ06OhnUOvVh-X3VVA2Uo0aaQ66M-CU`

Descomprimir en el directorio __attend2u/data/__.

7.Descargar archivos de test knn para hashtags y descomprimir.

`https://mega.nz/#!15NnzKQT!AoJ2iOhrawO53sX-GyL7tdvCEuqf3iN2WIqlP6_8fxQ`

Descomprimir en el directorio __knn-hashtags/__.

8.Descargar archivos de test knn para captions.

`https://mega.nz/#!s8cX2QbK!-5XSGhKCYR65BU8_q0ga86km1BMC_6DOfSGMhxJ57P8`

Descomprimir en el directorio __knn-captions/__.

## Estructura de directorios

```
├── api_twitter                                                                                                                                   
│   ├── datos_usuarios                                                                                                                            
│   ├── generartestconjunto                                                                                                                       
│   ├── generartestunico                                                                                                                          
│   └── imagenes-twitter                                                                                                                          
│       └── attend2u_examples_best_hash-1.png at master · cesc-park_attend2u_files                                                                
├── attend2u                                                                                                                                      
│   ├── assets                                                                                                                                    
│   ├── data                                                                                                                                      
│   │   ├── caption_dataset                                                                                                                       
│   │   ├── hashtag_dataset                                                                                                                       
│   │   │   ├── test2                                                                                                                             
│   │   │   └── tests                                                                                                                             
│   │   ├── images                                                                                                                                
│   │   ├── json                                                                                                                                  
│   │   └── resnet_pool5_features                                                                                                                 
│   ├── model
│   ├── scripts
│   └── utils
│       ├── pycocoevalcap
│       │   ├── bleu
│       │   ├── cider
│       │   ├── meteor
│       │   │   └── data
│       │   ├── rouge
│       │   └── tokenizer
│       └── pycocotools
├── knn-captions
│   ├── test-captions
│   │   └── original
│   │       └── caption_dataset
│   └── utils
│       ├── pycocoevalcap
│       │   ├── bleu
│       │   ├── cider
│       │   ├── meteor
│       │   │   └── data
│       │   ├── rouge
│       │   └── tokenizer
│       └── pycocotools
└── knn-hashtags
    ├── hashtag_dataset
    ├── knn-instagram
    ├── knn-instagram-twitter
    └── test-hashtags
        ├── original
        ├── testconjuntos
        └── testunicos
```

# Ejecutar el código

## API-Twitter

En caso de seguir los prerrequisitos este apartado se puede omitir para ahorrar tiempo al usuario, en caso de querer comprobar como se obtienen los datos de Twitter y se generan los ficheros de test ejecutar los siguientes comandos:

### Extraer y procesar la información

`cd api_twitter`

1.Descargar toda la información relevante de los usuarios de Twitter.

`python extraer_informacion_twitter.py`

2.Descargar las imágenes de los usuarios.

`python descargar_imagenes_tweet.py`

3.Ver estadísticas de los usuarios (opcional). 

`python estadisticas.py`

### Generar archivos de test

Existen dos tipos de pruebas: los usuarios únicos y las de conjuntos de usuarios de un mismo tipo, en caso de generar usuarios únicos nos situaremos en el directorio generartestunico/, y en caso de generar conjuntos de usuarios de un mismo tipo generartestconjunto/. 

1. En ambos casos la ejecución será la misma, nos situamos en el directorio y ejecutamos el script:

`./script`

Esto generara los ficheros de test que deberemos reemplazar en la librería attend2u para poder ejecutar las nuevas pruebas.

## Pruebas knn

### KNN hashtags
Situarse en el directorio __knn-hashtags/knn-instagram/__ o __knn-hashtags/knn-instagram-twitter/__, dependiendo de que datos se quieran usar y a continuación ejecutar los siguientes comandos:

`python knn_img_1.py`

`python knn_user_1.py`

`python knn_user_image.py`


### KNN captions
Situarse en el directorio __knn-captions/__ y a continuación ejecutar los siguientes comandos:

`python knn_img_1.py`

`python knn_user_1.py`

`python knn_user_image.py`

## Pruebas redes neuronales

### Modificaciones
Hemos realizado algunas modificaciones en los ficheros de la librería attend2u:
	
	eval.py
	train.py
	utils/data_utils.py
	utils/pycocoevalcap/evaluator.py
	scripts/generate_dataset.py
	

El resto de ficheros no ha sufrido ningún tipo de modificación, salvo los ficheros de test para hashtags donde hemos añadido y configurado los nuestros.

### Entrenamiento
Para el entrenamiento de la red neuronal nos dirigimos al directorio attend2u/ y ejecutamos el siguiente comando:

`python -m train --num_gpus 1 --batch_size 90`

### Evaluación
Para la evaluación de la red neuronal nos dirigimos al directorio attend2u/ y ejecutamos el siguiente comando:

`python -m eval --num_gpus 1 --batch_size 150`

Es importante recordar que para que funcione la métrica METEOR es necesario dar el siguiente valor a la variable LANG:

`LANG="en_US.UTF-8"`

#### Checkpoints

Por otro lado los .ckpt de las redes pre-entrenadas están en el siguiente enlace:

`https://mega.nz/#!so0wHAwL!3-UqchCHhXsVBwpcEy59QMbZGzMFJmbZsMsTGRIU3mg`

Una vez descargados elegimos uno de ellos y copiamos su directorio checkpoints dentro de la carpeta __attend2u/__.

Después configuramos el parámetro __max_context_length__, indicando la memoria de contexto que queremos dar a la red y ejecutamos el comando de evaluación.

`
flags.DEFINE_integer("max_context_length", XX,
    "User contex max length default [60]"
)
`
#### Caption o hashtags
En caso de querer entrenar o evaluar caption o hashtags tendremos que cambiar la configuración de la red, modificando los siguientes ficheros:

attend2u/utils/data_utils.py
	
	-- Cambiar las rutas, en vez de utilizar caption_dataset utilizamos hashtag_dataset o viceversa.
	-- Cambiamos el tamaño del diccionario de 40,000 a 60,0000 o viceversa.

attend2u/eval.py

	-- Cambiamos el path del diccionario.
	-- Descomentamos las líneas para utilizar las métricas COCO o F1_Score.

## Autores

[Ricardo Sánchez-Guzmán Hitti](mailto:ricardo.sanchez-guzman@estudiante.uam.es), [Alejandro Bellogín Kouki](http://ir.ii.uam.es/~alejandro/)