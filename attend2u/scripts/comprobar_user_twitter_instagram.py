# -*- coding: utf-8 -*-
import operator
import re
import os
import json
import logging
from collections import Counter
import requests
from tqdm import tqdm
import colorlog
from sklearn.feature_extraction.text import TfidfTransformer
import numpy as np
import httplib
import urlparse

#####################
# Hyperparameters
#####################
CONTEXT_LENGTH = 100
CAPTION_VOCAB_SIZE = 40000
HASHTAG_VOCAB_SIZE = 60000
DATA_ROOT_PATH = '../data'

# For dataset
CAPTION_TRAIN_JSON_FNAME = os.path.join(
    DATA_ROOT_PATH, 'json', 'insta-caption-train.json'
)
CAPTION_TEST1_JSON_FNAME = os.path.join(
    DATA_ROOT_PATH, 'json', 'insta-caption-test1.json'
)
CAPTION_TEST2_JSON_FNAME = os.path.join(
    DATA_ROOT_PATH, 'json', 'insta-caption-test2.json'
)
HASHTAG_TRAIN_JSON_FNAME = os.path.join(
    DATA_ROOT_PATH, 'json', 'insta-hashtag-train.json'
)
HASHTAG_TEST1_JSON_FNAME = os.path.join(
    DATA_ROOT_PATH, 'json', 'insta-hashtag-test1.json'
)
HASHTAG_TEST2_JSON_FNAME = os.path.join(
    DATA_ROOT_PATH, 'json', 'insta-hashtag-test2.json'
)

CAPTION_OUTPUT_PATH = os.path.join(DATA_ROOT_PATH, 'caption_dataset')
HASHTAG_OUTPUT_PATH = os.path.join(DATA_ROOT_PATH, 'hashtag_dataset')

CAPTION_VOCAB_FNAME = os.path.join(
    CAPTION_OUTPUT_PATH, '%d.vocab' % (CAPTION_VOCAB_SIZE)
)
HASHTAG_VOCAB_FNAME = os.path.join(
    HASHTAG_OUTPUT_PATH, '%d.vocab' % (HASHTAG_VOCAB_SIZE)
)

# For vocaulary
_PAD = "_PAD"
_GO = "_GO"
_EOS = "_EOS"
_UNK = "_UNK"
_START_VOCAB = [_PAD, _GO, _EOS, _UNK]

PAD_ID = 0
GO_ID = 1
EOS_ID = 2
UNK_ID = 3


dic_user =  set()

# For tokenization
try:
  # UCS-4
  EMOTICON = re.compile(u'(([\U00002600-\U000027BF])|([\U0001f300-\U0001f64F])|([\U0001f680-\U0001f6FF]))')
except Exception, e:
  # UCS-2
  EMOTICON = re.compile(u'(([\u2600-\u27BF])|([\uD83C][\uDF00-\uDFFF])|([\uD83D][\uDC00-\uDE4F])|([\uD83D][\uDE80-\uDEFF]))')
NOT_EMOTICON = re.compile(r'(\\U([0-9A-Fa-f]){8})|(\\u([0-9A-Fa-f]){4})')


def sort_dict(dic):
  # Sort by alphabet
  sorted_pair_list = sorted(dic.items(), key=operator.itemgetter(0))
  # Sort by count
  sorted_pair_list = sorted(sorted_pair_list, key=operator.itemgetter(1), reverse=True)
  return sorted_pair_list


def load_json(json_fname):
  colorlog.info("Load %s" % (json_fname))
  with open(json_fname, 'r') as f:
    json_object = json.load(f)
  return json_object


def tokenize(sentence):
  """Tokenize a sentence"""
  if isinstance(sentence, list):
    sentence = ' '.join(sentence)

  sentence = sentence.replace('#', ' #')
  sentence = sentence.replace('@', ' @')
  sentence = sentence.replace('\n', ' ')
  sentence = sentence.lower()
  sentence = re.sub(r'@[a-zA-Z0-9._]+', '@username', sentence)  # change username
  sentence = EMOTICON.sub(r'@@byeongchang\1 ', sentence)
  sentence = sentence.encode('unicode-escape')  # for emoticons
  sentence = re.sub(r'@@byeongchang\\', '@@byeongchang', sentence)
  sentence = NOT_EMOTICON.sub(r' ', sentence)
  sentence = re.sub(r"[\-_]", r"-", sentence)  # incoporate - and _
  sentence = re.sub(r"([!?,\.\"])", r" ", sentence)  # remove duplicates on . , ! ?
  sentence = re.sub(r"(?<![a-zA-Z0-9])\-(?![a-zA-Z0-9])", r"", sentence)  # remove - if there is no preceed or following
  sentence = ' '.join(re.split(r'[^a-zA-Z0-9#@\'\-]+', sentence))
  sentence = re.sub(r'@@byeongchang', r' \\', sentence)
  return sentence.split()


def tokenize_all(train_json, test1_json, test2_json, key='caption'):
  """
  Tokenize sentences in raw dataset

  Args:
    train_json, test1_json, test2_json: raw json object
    key: 'caption' or 'tags'
  """

  colorlog.info("Tokenize %s data" % (key))
  token_counter = Counter()
  train_tokens = {}
  test1_tokens = {}
  test2_tokens = {}

  # Train data
  for user_id, posts in tqdm(train_json.items(), ncols=70, desc="train data"):
  	for post_id, post in posts.items():
  		#print("User id_train ", post['user']['username'])
  		dic_user.add(post['user']['username'])

  # Test1 data
  for user_id, posts in tqdm(test1_json.items(), ncols=70, desc="test1 data"):
  	for post_id, post in posts.items():
  		#print("User id_train ", post['user']['username'])
  		dic_user.add(post['user']['username'])

  for user_id, posts in tqdm(test2_json.items(), ncols=70, desc="test2 data"):
  	for post_id, post in posts.items():
  		#print("User id_train ", post['user']['username'])
  		dic_user.add(post['user']['username'])



  return token_counter, train_tokens, test1_tokens, test2_tokens




def main():
  colorlog.basicConfig(
      filename=None,
      level=logging.INFO,
      format="%(log_color)s[%(levelname)s:%(asctime)s]%(reset)s %(message)s",
      datafmt="%Y-%m-%d %H:%M:%S"
  )

  if not os.path.exists(CAPTION_OUTPUT_PATH):
    colorlog.info("Create directory %s" % (CAPTION_OUTPUT_PATH))
    os.makedirs(CAPTION_OUTPUT_PATH)
  if not os.path.exists(HASHTAG_OUTPUT_PATH):
    colorlog.info("Create directory %s" % (HASHTAG_OUTPUT_PATH))
    os.makedirs(HASHTAG_OUTPUT_PATH)



  # Load raw data
  caption_train_json = load_json(CAPTION_TRAIN_JSON_FNAME)
  caption_test1_json = load_json(CAPTION_TEST1_JSON_FNAME)
  caption_test2_json = load_json(CAPTION_TEST2_JSON_FNAME)
  hashtag_train_json = load_json(HASHTAG_TRAIN_JSON_FNAME)
  hashtag_test1_json = load_json(HASHTAG_TEST1_JSON_FNAME)
  hashtag_test2_json = load_json(HASHTAG_TEST2_JSON_FNAME)

  # Tokenize all
  caption_counter, caption_train_tokens, caption_test1_tokens, \
      caption_test2_tokens = tokenize_all(
          caption_train_json,
          caption_test1_json,
          caption_test2_json,
          'caption'
      )
  hashtag_counter, hashtag_train_tokens, hashtag_test1_tokens, \
      hashtag_test2_tokens = tokenize_all(
          hashtag_train_json,
          hashtag_test1_json,
          hashtag_test2_json,
          'tags'
      )

  """
  print("Numero de usuarios totales" , len(dic_user))
  cont = 0

  f = open ('usuarios_validos.txt','w')
  for user in list(dic_user):
  	url = "https://twitter.com/"+user
  	req = requests.get(url)
	if(req.status_code == 200):
		cont +=1
		f.write(user+" \n")
  f.close()

  print("Numero de cuentas validas: ", cont)
  """

  

if __name__ == '__main__':
  main()
