# -*- coding: utf-8 -*-
import tweepy
import json
import re
import time
import os
import wget
from os import  getcwd
from os.path import abspath

from scandir import scandir
from shutil import copyfile
import shutil



def ls(ruta = getcwd()):
    return [abspath(arch.path) for arch in scandir(ruta) if arch.is_file()]


def generarUsuarioIntermedio():

	with open('../usuarios_hashtags_image.json') as file:
		data = json.load(file)

	cont = 0
	dic = {}
	usuarios = set()
	for username, tweets in data.items():
		usuarios.add(username)
		for tweet_id, tweet in tweets.items():
			if 'imagen' in data[username][tweet_id] and tweet_id != 'tags':
				cont+=1
				url =  tweet['imagen']
				id_tweet = tweet_id 
				usuario = username

				identificador_imagen = id_tweet + "_"+usuario
				dic[identificador_imagen] = url

	imagenes_user = set()

	numero_imagenes_descargadas = {}
	path_image = "../imagenes-twitter"
	paths = ls(path_image)
	for path in paths:	
		user = '_'.join(path.split("_")[2:])

		if user in imagenes_user:
			numero_imagenes_descargadas[user]+=1
		else:
			numero_imagenes_descargadas[user] = 1

		imagenes_user.add(user)

	
	flag = False

	for user in usuarios:
		if user not in imagenes_user:
			del data[user]

	numero_imagenes = 0
	urls = set()
	for username, tweets in data.items():
		urls = set()
		for tweet_id, tweet in tweets.items():
			if 'imagen' in tweet and tweet_id != 'tags':
				urls.add(tweet['imagen'])
				numero_imagenes+=1
		#print("Numero de imagenes totales", len(urls))
		#print("Numero de imagenes descargadas", numero_imagenes_descargadas[username])

		if len(urls) <= numero_imagenes_descargadas[username]:
			#print("Usuario ", username, " Numero de imagenes ", len(urls))
			
		# Tocar para coger un usuario con x imagenes
			if not flag and (len(urls)>250 and len(urls)<450 ):
				#print("Usuario ", username, " Numero de imagenes ", len(urls))
				flag = True
			else:
				del data[username]
	
		else:
			#print("Numero de urls ", len(urls), " Numero de imagenes descargadas ",  numero_imagenes_descargadas[username])
			del data[username]


	# Creamos el directorio de imagenes para test


	directorio = "imagenes_test1_intermedio/"
	try:
		shutil.rmtree(directorio)
	except:
		pass

	os.mkdir(directorio)
	
	for username, tweets in data.items():
		usuarios.add(username)
		for tweet_id, tweet in tweets.items():
			if 'imagen' in data[username][tweet_id] and tweet_id != 'tags':
			
				path = "../imagenes-twitter/"+tweet_id+"_"+ username
				
				try:
					copyfile(path, directorio+username+"_@_"+ tweet_id)
				except:
					media_file = tweet['imagen']
					wget.download(media_file, "../imagenes-twitter/"+tweet_id+"_"+ username)

					copyfile(path, directorio+username+"_@_"+ tweet_id)
				
	
	file_name = "test1-intermedio.json"
	
	with open( file_name, 'w') as file:
		json.dump(data, file)


if __name__ == "__main__":
	generarUsuarioIntermedio() 
