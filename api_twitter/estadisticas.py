# -*- coding: utf-8 -*-
import tweepy
import json
import re
import time
import os

from os import  getcwd
from os.path import abspath

from scandir import scandir

def ls(ruta = getcwd()):
    return [abspath(arch.path) for arch in scandir(ruta) if arch.is_file()]

def estadisticas_generales():
	stats = {}

	#patron = re.compile("\B(\#[a-zA-Z]+\b)(?!;)") 
	patron = re.compile('(?:\s|^)#[A-Za-z0-9\-\.\_]+(?:\s|$)')

			    
	for path in ls("datos_usuarios"):
		with open(path) as file:
		    data = json.load(file)
		    for key, value in  data.items():
				stats[key] = {}
				hashtag = []
				for tweet in value['tweets']:

					#print("Numero de hashtag", len(tweet[0]))

					hashtag.extend(patron.findall(tweet[0]))

				stats[key]['hashtag'] = hashtag

				for key2, value2 in value.items():
					stats[key][key2] = value2
			
		file.close()


	# Contadores de cada atributo
	imagenes = 0
	followers = 0
	following = 0
	hashtag = 0
	tweets = 0
	usuarios = 0

	# Valores de filtrado
	MAX_imagenes = 50
	MAX_followers = 500
	MAX_following = 500
	MAX_hashtag = 50
	MAX_tweets = 800


	for key, value in stats.items():
		usuarios+=1
		numero_imagenes = len(stats[key]['imagenes'])
		numero_followers = len(stats[key]['followers'])
		numero_following = len(stats[key]['following'])
		numero_hashtag = len(stats[key]['hashtag'])
		numero_tweets = len(stats[key]['tweets'])

		
		if numero_imagenes > MAX_imagenes:
			imagenes+=1

		if numero_followers > MAX_followers:
			followers+=1

		if numero_following > MAX_following:
			following+=1

		if numero_hashtag > MAX_hashtag:
			hashtag+=1

		if numero_tweets > MAX_tweets:
			tweets+=1	

	print("####################### Estadisticas usuarios ##########################")
	print("Numero total de usuarios ", usuarios)
	print("El numero total de usuarios con mas de ", MAX_imagenes," imagenes es: ", imagenes)		
	print("El numero total de usuarios con mas de ", MAX_followers," followers es: ", followers)		
	print("El numero total de usuarios con mas de ", MAX_following," following es: ", following)		
	print("El numero total de usuarios con mas de ", MAX_hashtag," hasthtag es: ", hashtag)		
	print("El numero total de usuarios con mas de ", MAX_tweets," tweets es: ", tweets)		
	print("########################################################################")

		


def usuarios_tweets_hashtags():
	with open('usuarios_hashtags_image.json') as file:
		data = json.load(file)
	NUM_HASHTAHS_IAMGE = 50
	cont_image_hashtag = 0 
	# Numero de tweets con imagenes y hashtags
	for key, value in data.items():
		#print(key, len(value))
		if len(value) >= NUM_HASHTAHS_IAMGE:
			cont_image_hashtag+=1

	print("################## Hashtags #####################")	
	print("Numero minimo de hashtags ", NUM_HASHTAHS_IAMGE)
	print("Numero de usuarios con imagenes y la cantidad de hashtags indicados",  cont_image_hashtag)
	print("#################################################")	

def numero_total_tweets():
	patron = re.compile('(?:\s|^)#[A-Za-z0-9\-\.\_]+(?:\s|$)')
	file_name = "total_usuarios.json"
	stats = {}
	total_tweets = 0
	total_hashtags = 0
	with open(file_name, 'w') as file2:
				    
			for path in ls("datos_usuarios"):
				with open(path) as file:
				    data = json.load(file)
				    for key, value in  data.items():
						stats[key] = {}
						hashtag = []

						#print("Tweets ", len(value['tweets']))
						total_tweets += len(value['tweets'])

						for tweet in value['tweets']:

							hashtag.extend(patron.findall(tweet[0]))

						total_hashtags+=len(hashtag)
						stats[key]['hashtag'] = hashtag

						for key2, value2 in value.items():
							stats[key][key2] = value2
					
				file.close()
	print("################## Numero de Tweets #####################")			
	print("Numero total de tweets con imagenes", total_tweets)
	print("Numero total de hashtag con imagenes ", total_hashtags)
	print("User/tweets ", (1.0 * total_tweets)/( 1.0 * 1115))
	print("Media ", ( 1.0 * total_tweets)/(1.0 * total_hashtags))
	print("#########################################################")


if __name__ == "__main__":
    estadisticas_generales()
    usuarios_tweets_hashtags()
    numero_total_tweets()