# -*- coding: utf-8 -*-
import tweepy
import json
import time
import os
import wget
from os import  getcwd
from os.path import abspath
from scandir import scandir
from tqdm import tqdm

def ls(ruta = getcwd()):
		return [abspath(arch.path) for arch in scandir(ruta) if arch.is_file()]



def descargar_imagenes():

	# Cargamos el fichero JSON con todas las urls de las imagenes de los usuarios
	with open('usuarios_hashtags_image.json') as file:
			data = json.load(file)

	cont = 0
	dic = {}

	# En caso de ya existir imagenes en el directorio las cargamos
	path_image = "imagenes-twitter"
	paths = []
	try:
		paths = ls(path_image)
	except:
		os.mkdir(path_image)

	rutas = set()

	for path in paths:
		rutas.add(path)  

	for username, tweets in tqdm(data.items()):
		for tweet_id, tweet in tweets.items():
			if 'imagen' in tweet and tweet_id != 'tags':

				url =  tweet['imagen']
				id_tweet = tweet_id 
				usuario = username	
				# Omitimos imagenes repetidas
				if url not in rutas:
					# Guardamos las imagenes dentro del directorio
					identificador_imagen = "imagenes-twitter/"+id_tweet +"_"+usuario
					print(identificador_imagen)
					print(url)
					wget.download(url, identificador_imagen)
					rutas.add(url)


if __name__ == "__main__":
		descargar_imagenes() 
