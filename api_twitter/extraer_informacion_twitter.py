# -*- coding: utf-8 -*-
import tweepy
import json
import re
import time
import os

# For tokenization
try:
  # UCS-4
  EMOTICON = re.compile(u'(([\U00002600-\U000027BF])|([\U0001f300-\U0001f64F])|([\U0001f680-\U0001f6FF]))')
except Exception, e:
  # UCS-2
  EMOTICON = re.compile(u'(([\u2600-\u27BF])|([\uD83C][\uDF00-\uDFFF])|([\uD83D][\uDC00-\uDE4F])|([\uD83D][\uDE80-\uDEFF]))')
NOT_EMOTICON = re.compile(r'(\\U([0-9A-Fa-f]){8})|(\\u([0-9A-Fa-f]){4})')


def extraerInformacionTwitter():

	
	# Credenciales para la validacion en Twitter

	consumer_token = "jZlfjwe3eniACk5IeU32g33Yr"
	consumer_secret= "pmPcGJWGOkVwtEmpZx1gHwa7rgTzfOy498NM4C0nakpWtCWxJR"

	auth = tweepy.OAuthHandler(consumer_token, consumer_secret)
	access_token ="1110230481239449600-HhfYEdHtP2CjIh6OTko8iUKrR5ezPL"

	access_token_secret ="CaKYggLNTFLov41BN8eWBQ5sOKAoZ16GKMHvUngJEg1Ve"

	auth.set_access_token(access_token, access_token_secret)


	try:
		redirect_url = auth.get_authorization_url()

	except tweepy.TweepError:
		print 'Error! Failed to get request token.'

	api = tweepy.API(auth, wait_on_rate_limit=True)

	# Creamos los directorios 
	#############################################
	#cada directorio tiene el nombre del usuario#
	#############################################


	# Leemos los usuarios que existen en ambos dominios y son publicos
	with open('Usuarios_validos_public_sinProcesar.txt') as f:
	    content = f.readlines()

	usuarios = [x.strip() for x in content] 

	dic = {}

	for usuario in usuarios:
		dic[usuario] = {}
		print("Usuario", usuario )


		# Descargar los tweets de un usuario y las urls
		tweets = api.user_timeline(screen_name=usuario,
		                           count=200, include_rts=False,
		                           exclude_replies=True, tweet_mode="extended")
		
		last_id = 0
		if(len(tweets)>0):
			last_id = tweets[-1].id
		
		while (True):
			more_tweets = api.user_timeline(screen_name=usuario,
		                                count=200,
		                                include_rts=False,
		                                exclude_replies=True,
		                                max_id=last_id-1, tweet_mode="extended")
			# En caso de que el usuario no tenga más tweets
			if (len(more_tweets) == 0):
				break
			else:
				last_id = more_tweets[-1].id-1
				tweets = tweets + more_tweets

		media_files = set()
		hashtags_images = set()

		for status in tweets:

			media = status.entities.get('media', [])
			# En caso de que tenga alguna URL
			if(len(media) > 0):
				id_str = media[0]['id_str']
				hashtags = status.entities.get('hashtags', [])
				# En caso de que tenga algun hashtag
				if(len(hashtags)>0):
					media_files.add(media[0]['media_url'])
					dic[usuario][id_str] = {}
					# Guardamos en un diccionario la imagen y los hashtags vinculados a esa imagen
					dic[usuario][id_str]['imagen'] = media[0]['media_url']
					dic[usuario][id_str]['tags_imagen'] = [ h['text'] for h in hashtags]


		

	file_name = "usuarios_hashtags_image.json"


	# Volcamos toda la informacion en un fichero .json
	
	with open( file_name, 'w') as file:
		json.dump(data, file)
	
	# Descomentar en caso de guardar un archivo json por cada usuario
	"""
      dic[usuario]['imagen']={}
        
		for m_f in media_files:
			print(m_f)

		# La descarga de imagenes se realiza en descargar_imagenes_tweet.py
		
		#for media_file in media_files:
		#	wget.download(media_file)
		
		tweets_for_csv = [[tweet.full_text] for tweet in tweets] # CSV file created  
		
		tmp=[]  
		for tweet in tweets_for_csv: 
			
			# Parseamos las palabras
			tweet = tokenize(tweet)
			tmp.append(' '.join(tweet))  
	  
		data = {}

		data[usuario] = {}

		data[usuario]['followers'] = username_followers_id
		data[usuario]['following'] = username_friends_id
		data[usuario]['imagenes'] = list(media_files)
		data[usuario]['tweets'] = tweets_for_csv

		file_name = "datos_usuarios/"+usuario+".json"

		with open( file_name, 'w') as file:
		    json.dump(data, file)
	"""
	
def tokenize(sentence):
  """Tokenize a sentence"""
  if isinstance(sentence, list):
    sentence = ' '.join(sentence)

  sentence = sentence.replace('#', ' #')
  sentence = sentence.replace('@', ' @')
  sentence = sentence.replace('\n', ' ')
  sentence = sentence.lower()
  sentence = re.sub(r'@[a-zA-Z0-9._]+', '@username', sentence) 
  sentence = EMOTICON.sub(r'@@byeongchang\1 ', sentence)
  sentence = sentence.encode('unicode-escape')  
  sentence = re.sub(r'@@byeongchang\\', '@@byeongchang', sentence)
  sentence = NOT_EMOTICON.sub(r' ', sentence)
  sentence = re.sub(r"[\-_]", r"-", sentence) 
  sentence = re.sub(r"([!?,\.\"])", r" ", sentence) 
  sentence = re.sub(r"(?<![a-zA-Z0-9])\-(?![a-zA-Z0-9])", r"", sentence) 
  sentence = ' '.join(re.split(r'[^a-zA-Z0-9#@\'\-]+', sentence))
  sentence = re.sub(r'@@byeongchang', r' \\', sentence)
  return sentence.split()

if __name__ == "__main__":
    extraerInformacionTwitter() 
