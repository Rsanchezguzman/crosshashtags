# -*- coding: utf-8 -*-
import tweepy
import json
import re
import time
import os
import wget
from os import  getcwd
from os.path import abspath

from scandir import scandir
from shutil import copyfile
import shutil



def ls(ruta = getcwd()):
    return [abspath(arch.path) for arch in scandir(ruta) if arch.is_file()]

# For tokenization
try:
  # UCS-4
  EMOTICON = re.compile(u'(([\U00002600-\U000027BF])|([\U0001f300-\U0001f64F])|([\U0001f680-\U0001f6FF]))')
except Exception, e:
  # UCS-2
  EMOTICON = re.compile(u'(([\u2600-\u27BF])|([\uD83C][\uDF00-\uDFFF])|([\uD83D][\uDC00-\uDE4F])|([\uD83D][\uDE80-\uDEFF]))')
NOT_EMOTICON = re.compile(r'(\\U([0-9A-Fa-f]){8})|(\\u([0-9A-Fa-f]){4})')


def generarConjuntoUsuariosPequeno():

	with open('../usuarios_hashtags_image.json') as file:
		data = json.load(file)

	cont = 0
	dic = {}
	usuarios = set()
	for username, tweets in data.items():
		usuarios.add(username)
		for tweet_id, tweet in tweets.items():
			if 'imagen' in data[username][tweet_id] and tweet_id != 'tags':
				cont+=1
				url =  tweet['imagen']
				id_tweet = tweet_id 
				usuario = username

				identificador_imagen = id_tweet + "_"+usuario
				dic[identificador_imagen] = url

	imagenes_user = set()


	numero_imagenes_descargadas = {}
	path_image = "../imagenes-twitter"
	paths = ls(path_image)
	for path in paths:	
		user = '_'.join(path.split("_")[2:])

		if user in imagenes_user:
			numero_imagenes_descargadas[user]+=1
		else:
			numero_imagenes_descargadas[user] = 1

		imagenes_user.add(user)

	flag = False

	for user in usuarios:
		if user not in imagenes_user:
			del data[user]

	numero_imagenes = 0
	cont = 0
	for username, tweets in data.items():
		urls = set()
		for tweet_id, tweet in tweets.items():
			if 'imagen' in tweet and tweet_id != 'tags':
				urls.add(tweet['imagen'])
				numero_imagenes+=1

		if len(urls) <= numero_imagenes_descargadas[username]:

		# Tocar para coger un usuario con x imagenes
			if not flag and (len(urls)>=1 and len(urls)<170 ):
				#print("Usuario ", username, " Numero de imagenes ", len(urls))
				cont+=len(urls)
				if cont>=4000:
					flag = True
			else:
				del data[username]
		else:
			del data[username]


	# Creamos el directorio de imagenes para test

	directorio = "imagenes_test1_pequeno/"
	try:
		shutil.rmtree(directorio)
	except:
		pass

	os.mkdir(directorio)
	
	for username, tweets in data.items():
		usuarios.add(username)
		for tweet_id, tweet in tweets.items():
			if 'imagen' in data[username][tweet_id] and tweet_id != 'tags':
			

				path = "../imagenes-twitter/"+tweet_id+"_"+ username

				try:
					if os.stat(path).st_size == 0:
						del data[username][tweet_id]
					else:
						copyfile(path, directorio+username+"_@_"+ tweet_id)
				except:
					media_file = tweet['imagen']
					wget.download(media_file, "../imagenes-twitter/"+tweet_id+"_"+ username)
					if os.stat(path).st_size == 0:
						del data[username][tweet_id]

					else:
						copyfile(path, directorio+username+"_@_"+ tweet_id)
			
	file_name = "test1-pequeno.json"
	
	with open( file_name, 'w') as file:
		json.dump(data, file)

if __name__ == "__main__":
	generarConjuntoUsuariosPequeno() 
