import operator
import re
import os
import json
import logging
from collections import Counter
from sklearn.neighbors import KNeighborsClassifier
import glob
from tqdm import tqdm
import colorlog
import warnings
warnings.filterwarnings("ignore")
from sklearn.feature_extraction.text import TfidfTransformer
import numpy as np
from os import listdir
from sklearn.metrics import f1_score

#####################
# Hyperparameters
#####################
CONTEXT_LENGTH = 100

HASHTAG_VOCAB_SIZE = 60000
DATA_ROOT_PATH = '../test-hashtags/original'

# For dataset
HASHTAG_TRAIN_JSON_FNAME = os.path.join(
    "../test-hashtags/", 'insta-hashtag-train.json'
)
HASHTAG_TEST1_JSON_FNAME = os.path.join(
    DATA_ROOT_PATH,  'insta-hashtag-test1.json'
)

HASHTAG_OUTPUT_PATH = os.path.join( '../hashtag_dataset')

HASHTAG_VOCAB_FNAME = os.path.join(
    HASHTAG_OUTPUT_PATH, '%d.vocab' % (HASHTAG_VOCAB_SIZE)
)

# For vocaulary
_PAD = "_PAD"
_GO = "_GO"
_EOS = "_EOS"
_UNK = "_UNK"
_START_VOCAB = [_PAD, _GO, _EOS, _UNK]

PAD_ID = 0
GO_ID = 1
EOS_ID = 2
UNK_ID = 3

# For tokenization
try:
  # UCS-4
  EMOTICON = re.compile(u'(([\U00002600-\U000027BF])|([\U0001f300-\U0001f64F])|([\U0001f680-\U0001f6FF]))')
except Exception, e:
  # UCS-2
  EMOTICON = re.compile(u'(([\u2600-\u27BF])|([\uD83C][\uDF00-\uDFFF])|([\uD83D][\uDC00-\uDE4F])|([\uD83D][\uDE80-\uDEFF]))')
NOT_EMOTICON = re.compile(r'(\\U([0-9A-Fa-f]){8})|(\\u([0-9A-Fa-f]){4})')


def sort_dict(dic):
  # Sort by alphabet
  sorted_pair_list = sorted(dic.items(), key=operator.itemgetter(0))
  # Sort by count
  sorted_pair_list = sorted(sorted_pair_list, key=operator.itemgetter(1), reverse=True)
  return sorted_pair_list


def load_json(json_fname):
  colorlog.info("Load %s" % (json_fname))
  with open(json_fname, 'r') as f:
    json_object = json.load(f)
  return json_object


def tokenize(sentence):
  """Tokenize a sentence"""
  if isinstance(sentence, list):
    sentence = ' '.join(sentence)

  sentence = sentence.replace('#', ' #')
  sentence = sentence.replace('@', ' @')
  sentence = sentence.replace('\n', ' ')
  sentence = sentence.lower()
  sentence = re.sub(r'@[a-zA-Z0-9._]+', '@username', sentence)  # change username
  sentence = EMOTICON.sub(r'@@byeongchang\1 ', sentence)
  sentence = sentence.encode('unicode-escape')  # for emoticons
  sentence = re.sub(r'@@byeongchang\\', '@@byeongchang', sentence)
  sentence = NOT_EMOTICON.sub(r' ', sentence)
  sentence = re.sub(r"[\-_]", r"-", sentence)  # incoporate - and _
  sentence = re.sub(r"([!?,\.\"])", r" ", sentence)  # remove duplicates on . , ! ?
  sentence = re.sub(r"(?<![a-zA-Z0-9])\-(?![a-zA-Z0-9])", r"", sentence)  # remove - if there is no preceed or following
  sentence = ' '.join(re.split(r'[^a-zA-Z0-9#@\'\-]+', sentence))
  sentence = re.sub(r'@@byeongchang', r' \\', sentence)
  return sentence.split()


def tokenize_all(train_json, test1_json, key='tags'):
  """
  Tokenize sentences in raw dataset

  Args:
    train_json, test1_json: raw json object
    key: 'caption' or 'tags'
  """

  colorlog.info("Tokenize %s data" % (key))
  token_counter = Counter()
  train_tokens = {}
  test1_tokens = {}

  # Train data
  for user_id, posts in tqdm(train_json.items(), ncols=70, desc="train data"):
    train_tokens[user_id] = {}
    for post_id, post in posts.items():
      post_tokens = tokenize(post[key])
      train_tokens[user_id][post_id] = post_tokens
      for post_token in post_tokens:
        token_counter[post_token] += 1

  # Test1 data
  for user_id, posts in tqdm(test1_json.items(), ncols=70, desc="test1 data"):
    test1_tokens[user_id] = {}
    for post_id, post in posts.items():
      post_tokens = tokenize(post[key])
      test1_tokens[user_id][post_id] = post_tokens

  return token_counter, train_tokens, test1_tokens


def get_tfidf_words(train_tokens, test1_tokens,vocab, rev_vocab):
  colorlog.info("Get tfidf words")
  def _preprocess(all_tokens, rev_vocab):
    counter = np.zeros([len(all_tokens), len(rev_vocab)])
    user_ids = []
    for i, (user_id, posts) in enumerate(
        tqdm(all_tokens.items(), ncols=70, desc="preprocess")
    ):
      user_ids.append(user_id)
      for post_id, tokens in posts.items():
        token_ids = [rev_vocab.get(token, UNK_ID) for token in tokens]
        for token_id in token_ids:
          counter[i, token_id] += 1
    return counter, user_ids
  
  #####################
  # KNN PARA USUARIOS #
  #####################
  train_counter, train_user_ids = _preprocess(train_tokens, rev_vocab)
  test1_counter, test1_user_ids = _preprocess(test1_tokens, rev_vocab)

  hashtag_test_json = load_json(HASHTAG_TEST1_JSON_FNAME)
  test1_tokens = {}
  
  # Test1 data
  for user_id, posts in tqdm(hashtag_test_json.items(), ncols=70, desc="test1 data"):
    test1_tokens[user_id] = {}
    for post_id, post in posts.items():
      post_tokens = tokenize(post['tags'])

      test1_tokens[user_id][post_id] = post_tokens

  # Tratamos el vocabulario de usuario para poder usarlo en knn
  colorlog.info("Fit and transform train tfidf")
  vectorizer = TfidfTransformer()
  train_tfidf = vectorizer.fit_transform(train_counter).toarray()
  test1_tfidf = vectorizer.transform(test1_counter).toarray()

  with open("../test-hashtags/original/test1.txt", "r") as archivo_palabras:
     contenido = archivo_palabras.read()
     
  longitud_test = len(contenido.splitlines())
  matriz_test= range(longitud_test)
  clase_test = range(longitud_test)

  name_file_test = contenido.splitlines()
 
  clf = KNeighborsClassifier(n_neighbors=1)
  clf.fit(train_tfidf, train_user_ids)

  # Cargamos los hashtags de las imagenes
  #Test1 data
  with open(HASHTAG_TEST1_JSON_FNAME, 'r') as f: 
    test1_json = json.load(f)

  for user_id, posts in tqdm(test1_json.items(), ncols=70, desc="test1 data"):
    test1_tokens[user_id] = {}
    for post_id, post in posts.items():
      post_tokens =  post['tags']
      test1_tokens[user_id][post_id] = tokenize(post_tokens)

  # Cogemos todas las imagenes realacionadas con ese usuario
  # train data
  hashtag_train_json = load_json(HASHTAG_TRAIN_JSON_FNAME)

  token_counter = Counter()
  train_tokens = {}
  aux = ""
  for user_id, posts in tqdm(hashtag_train_json.items(), ncols=70, desc="test1 data"):
    train_tokens[user_id] = {}
    for post_id, post in posts.items():
      post_tokens = tokenize(post['tags'])
      train_tokens[user_id][post_id] = post_tokens

  descripciones_objetivo = []
  descripciones_finales = []

  for i,linea in enumerate(name_file_test):
    print("test  ", i)
    name_imagen_predecir = linea.split(",")[0]
    usuario = name_imagen_predecir.split("_")[0]
    id_imagen_predecir = "_".join(name_imagen_predecir.split("_")[2:5]).replace(".npy","")

    descripciones_objetivo.append(test1_tokens[usuario][id_imagen_predecir])

    # Cogemos el vocabulario de ese usuario
    indice_vocabulario_usuario = test1_user_ids.index(str(usuario).decode("utf-8"))
    
    # Vocabulario del usuario de la query
    vocabulario_usuario_query = test1_tfidf[indice_vocabulario_usuario]

    # Buscamos al usuario mas cercano al nuestro
    pred = clf.predict(vocabulario_usuario_query)
  
    #pred = clf.predict(np.array(vocabulario_Activo_60_usuario))
    id_usuario_cercano_unicode = str(int(pred[0])).decode("utf-8")

    # Post relacionados con ese usuario
    post_path = train_tokens[id_usuario_cercano_unicode].keys()

    #####################
    # KNN PARA IMAGENES #
    #####################

    array1 = range(len(post_path))
    array2 = range(len(post_path))
    for i, name in enumerate(post_path):
    	  path_imagen_completa = "../../attend2u/data/resnet_pool5_features/"+id_usuario_cercano_unicode+"_@_"+name+".npy"
  	  array1[i] = np.load(path_imagen_completa)
  	  array2[i] = name

    array1 = np.array(array1) 
    
    nsamples, nx, ny, nz = array1.shape
    d2_train_dataset = array1.reshape((nsamples,nx*ny*nz))  

    # Aprendizaje supervisado
    knn = KNeighborsClassifier(n_neighbors = 1)
    knn.fit(d2_train_dataset, array2)

    # Cargamos la imagen de la consulta
    path_imagen_completa = "../../attend2u/data/resnet_pool5_features/"+name_imagen_predecir
    
    imagen_predecir = np.load(path_imagen_completa)
    
    nsamples, nx, ny=  imagen_predecir.shape
    d2_test_dataset = imagen_predecir.reshape((1,2048)) 

    predi = knn.predict(d2_test_dataset)[0]
    
    descripciones_finales.append(train_tokens[id_usuario_cercano_unicode][predi])

  print("F1_score automatico ", f1_score(descripciones_objetivo, descripciones_finales, average='weighted'))


def create_vocabulary(counter, fname, vocab_size):
  colorlog.info("Create vocabulary %s" % (fname))
  sorted_tokens = sort_dict(counter)
  vocab = _START_VOCAB + [x[0] for x in sorted_tokens]
  if len(vocab) > vocab_size:
    vocab = vocab[:vocab_size]
  with open(fname, 'w') as f:
    for w in vocab:
      f.write(w + "\n")

  rev_vocab = {}
  for i, token in enumerate(vocab):
    rev_vocab[token] = i

  return vocab, rev_vocab

def main():
  colorlog.basicConfig(
      filename=None,
      level=logging.INFO,
      format="%(log_color)s[%(levelname)s:%(asctime)s]%(reset)s %(message)s",
      datafmt="%Y-%m-%d %H:%M:%S"
  )

  # Load raw data
  hashtag_train_json = load_json(HASHTAG_TRAIN_JSON_FNAME)
  hashtag_test1_json = load_json(HASHTAG_TEST1_JSON_FNAME)
 

  # Tokenize all
  hashtag_counter, hashtag_train_tokens, hashtag_test1_tokens = tokenize_all(
          hashtag_train_json,
          hashtag_test1_json,
          'tags'
      )

  # Create vocabulary
  hashtag_vocab, hashtag_rev_vocab = create_vocabulary(
      hashtag_counter, HASHTAG_VOCAB_FNAME, HASHTAG_VOCAB_SIZE
  )

  # Get tfidf weighted tokens
  get_tfidf_words(
          hashtag_train_tokens,
          hashtag_test1_tokens,
          hashtag_vocab,
          hashtag_rev_vocab
      )  

if __name__ == '__main__':
  main()
