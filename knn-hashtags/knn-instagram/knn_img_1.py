import numpy as np
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neighbors import NearestNeighbors
from scipy.spatial import distance
from os import listdir
from utils.evaluator import Evaluator
from sklearn.metrics import f1_score
import warnings
warnings.filterwarnings("ignore")
import re
import json
from tqdm import tqdm

# For tokenization
try:
  # UCS-4
  EMOTICON = re.compile(u'(([\U00002600-\U000027BF])|([\U0001f300-\U0001f64F])|([\U0001f680-\U0001f6FF]))')
except Exception, e:
  # UCS-2
  EMOTICON = re.compile(u'(([\u2600-\u27BF])|([\uD83C][\uDF00-\uDFFF])|([\uD83D][\uDC00-\uDE4F])|([\uD83D][\uDE80-\uDEFF]))')
NOT_EMOTICON = re.compile(r'(\\U([0-9A-Fa-f]){8})|(\\u([0-9A-Fa-f]){4})')

def main():
  
  # Cambiar para caption o hastags
  train_tokens = {}
  test1_tokens = {}

  with open("../test-hashtags/insta-hashtag-train.json", 'r') as f:
    train_json = json.load(f)
  with open("../test-hashtags/original/insta-hashtag-test1.json", 'r') as f: 
    test1_json = json.load(f)


  # Train data
  for user_id, posts in tqdm(train_json.items(), ncols=70, desc="train data"):
    train_tokens[user_id] = {}
    for post_id, post in posts.items():
      post_tokens = post['tags']
      train_tokens[user_id][post_id] = tokenize(post_tokens)

  # Test1 data
  for user_id, posts in tqdm(test1_json.items(), ncols=70, desc="test1 data"):
    test1_tokens[user_id] = {}
    for post_id, post in posts.items():
      post_tokens =  post['tags']
      test1_tokens[user_id][post_id] = tokenize(post_tokens)
  
  with open("../test-hashtags/train.txt", "r") as archivo_palabras:
     contenido = archivo_palabras.read()

  #longitud_train = len(contenido.splitlines())

  n_train = 600000
  matriz_train = range(n_train)
  clase_train = range(n_train)

  name_files_train = contenido.splitlines()[:n_train]

  for i,linea in enumerate(name_files_train):
    print("train ", i)
    name = linea.split(",")[0]
    matriz_train[i] = np.load('../../attend2u/data/resnet_pool5_features/' + name)
    clase_train[i] = name
   
  # TODO cambiar la siguiente linea por el test que queramos ejecutar
  with open("../test-hashtags/original/test1.txt", "r") as archivo_palabras:
     contenido = archivo_palabras.read()

  longitud_test = len(contenido.splitlines())
  matriz_test= range(longitud_test)
  clase_test = range(longitud_test)

  name_file_test = contenido.splitlines()

  for i,linea in enumerate(name_file_test):
    name = linea.split(",")[0]
    matriz_test[i] = np.load('../../attend2u/data/resnet_pool5_features/' + name)
    clase_test[i] = name
  
  train_d = np.array(matriz_train)
  test_d = np.array(matriz_test) 
  

  nsamples, nx, ny, nz = train_d.shape
  d2_train_dataset = train_d.reshape((nsamples,nx*ny*nz))  
 
  # Aprendizaje supervisado
  knn = KNeighborsClassifier(n_neighbors = 1)
  knn.fit(d2_train_dataset, clase_train)

  nsamples, nx, ny, nz = test_d.shape
  d2_test_dataset = test_d.reshape((nsamples,nx*ny*nz))  
 
  desc_token_list = []
  answer_token_list = []

  for i, d2_test in enumerate(d2_test_dataset):
    print("test ", i)
    predi = knn.predict(d2_test)
    name_prediccion = predi[0]
    user_id_train = name_prediccion.split("_")[0]
    post_id_train = "_".join(name_prediccion.split("_")[2:5]).replace(".npy","")

    name_target = clase_test[i]
    user_id_test = name_target.split("_")[0]
    post_id_test  = "_".join(name_target.split("_")[2:5]).replace(".npy","")

    desc_token_list.append(train_tokens[user_id_train][post_id_train])
    answer_token_list.append(test1_tokens[user_id_test][post_id_test])  
     
  print("F1_score automatico ", f1_score(answer_token_list, desc_token_list, average='weighted'))
  
  
def tokenize(sentence):
  """Tokenize a sentence"""
  if isinstance(sentence, list):
    sentence = ' '.join(sentence)

  sentence = sentence.replace('#', ' #')
  sentence = sentence.replace('@', ' @')
  sentence = sentence.replace('\n', ' ')
  sentence = sentence.lower()
  sentence = re.sub(r'@[a-zA-Z0-9._]+', '@username', sentence)  # change username
  sentence = EMOTICON.sub(r'@@byeongchang\1 ', sentence)
  sentence = sentence.encode('unicode-escape')  # for emoticons
  sentence = re.sub(r'@@byeongchang\\', '@@byeongchang', sentence)
  sentence = NOT_EMOTICON.sub(r' ', sentence)
  sentence = re.sub(r"[\-_]", r"-", sentence)  # incoporate - and _
  sentence = re.sub(r"([!?,\.\"])", r" ", sentence)  # remove duplicates on . , ! ?
  sentence = re.sub(r"(?<![a-zA-Z0-9])\-(?![a-zA-Z0-9])", r"", sentence)  # remove - if there is no preceed or following
  sentence = ' '.join(re.split(r'[^a-zA-Z0-9#@\'\-]+', sentence))
  sentence = re.sub(r'@@byeongchang', r' \\', sentence)
  return sentence.split()

  
if __name__== "__main__":
  main()


